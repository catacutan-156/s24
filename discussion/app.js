// natural and simple - reason why the logo of MongoDB is a leaf

// Node.js -> will provide us with a runtime environment (RTE) that will allow us to execute our program.

// RTE (Runtime Environment) -> is the environment or system in which a program can be executed.

// TASK: Let's create a standard server setup using plain NodeJS

// Identify and prepare components/ingredients needed in order to execute the task.
	// http (hypertext transfer protocol)
	// http -> is a built in module of NodeJS that will allow us to establish a connection and allow us to transfer data over the HTTP.

	// use a function called require() in order to acquire the desired module. Repackage the module and store it inside a new variable.
let http = require("http");

	// http will provide us with all the components needed to establish a server.

	// createServer() -> will allow us to create an HTTP server.

	// provide/instruct the server what to do.
	 //print a simple message. 

	// Identify and describe a location where the connection will happen, in order to provide a proper medium for both parties, and bind the connection to the desired port number.
let port = 3000;

	// listen() -> bind and listen to a specific port whenever its being accessed by your computer.

	// identify a point where we want to terminate the transmission using the end() function.

http.createServer(function(request, response) {
	response.write("Hello Batch 145! Welcome to Backend!");
	response.end() // terminate the transmission
}).listen(port);

	// create a response in the terminal to confirm if a connection has been successfully established.
		// 1. we can give a confirmation to the client that a connection is working.
		// 2. we can specify what address/location the connection was established.
	  // we will use template literals in order utilize placeholders.

console.log(`Server is running with nodemon on port: ${port}`); 

	// What can we do for us to be able to automaticall hotfix the system without restarting the server?

 		// 1. Initialize an npm into your local project.
 			// syntax: npm init
 			// shortcut syntax: npm init -y (yes)

 			// package.json -> "the Heart of any Node projects". It records all the important metadata about the project, (libraries, packages, functions) that makes up the system.

 		// 2. Install a dependency called "nodemon" into our project
 			// PRACTICE SKILLS:
 				// install:
 					// npm install <dependency>
 				// uninstall:
 					// npm uninstall <dependency>
 					// npm un <dependency>
 				// installing multiple dependencies:
 					// npm i <list of dependencies>
 						// express

 		// 3. run your app using the nodemon utility engine.
 			// note: make sure that nodemon is recognized by your file system structure

 			// npm install -g nodemon (global)
 				// -> the library is being installed "globally" into your machine's file system so that the program will become recognizable across your file system structure.
 				// (YOU ONLY HAVE TO EXECUTE THIS COMMAND ONCE)

 			// nodemon app.js

 			// nodemon utility -> is a CLI utility tool. Its task is to wrap your node js app and watch for any changes in the file system and automatically restarts/hotfix the process.

 			// create your personalized script to execute the app using the nodemon utility
 				// register a new command to start the app.

 				// start -> common execute script for node apps.

 				// others -> run + command

// [SEGWAY] Register Sublime Text as part of your environment variables.

	// 1. Locate in your PC where Sublime Text is installed.

	// 2. Copy the path where Sublime Text is installed.

	// 3. Locate the environment variables in your system

	// 4. Insert the path of ST3 under "Path" of the system environment