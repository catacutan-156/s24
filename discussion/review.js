// use case of .gitignore

	// Notes: You will be able to successfully push your node_modules in your remote repo.

	// Repercussions

	// -> it will take a longer time for you to stage and push your projects online.
	// -> it will cause an error during deployment because most hosting platforms reject node_modules during the building stage.

	// HOW TO FIX?
		// git rm -r --cached node_modules
		// rm = (remove), -r = recursive, --cached = memory, node_modules = folder to target
		// recursive -> involving a program or routine that described explicit intepretations of a component/module/file.